depends:
	bash src/depends.sh

config:
	bash src/copy-config.sh

clean-depends:
	bash src/clean-depends.sh

clean:
	bash src/clean.sh

menu:
	bash src/menu.sh

tiny:
	bash src/tiny.sh


download: clean-depends depends tiny config menu

floppy:
	bash src/floppy.sh

qemu:
	bash src/qemu.sh

build: clean  
	bash src/build.sh

all: build
