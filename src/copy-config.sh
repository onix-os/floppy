#!/bin/bash

source src/env.sh

cp $SRC/config/linux-config $SRC/linux/.config
cp $SRC/config/busybox-config $SRC/busybox/.config