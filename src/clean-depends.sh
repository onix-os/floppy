#!/bin/bash

source src/env.sh


if [ -d $SRC/linux ]
then
rm -rf $SRC/linux
fi

if [ -d $SRC/busybox ]
then
rm -rf $SRC/busybox/
fi

if [ -d $SRC/i486-linux-musl-cross ]
then
rm -rf $SRC/i486-linux-musl-cross
fi

