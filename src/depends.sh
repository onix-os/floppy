#!/bin/bash

source src/env.sh

cd $SRC
if [ ! -d $SRC/linux ]
then
git clone --depth=1 https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
fi

if [ ! -d "$SRC/busybox-1.33.1" ]
then
wget https://busybox.net/downloads/busybox-1.33.1.tar.bz2 
tar xjvf busybox-1.33.1.tar.bz2
mv busybox-1.33.1 busybox
rm busybox-1.33.1.tar.bz2
fi

if [ ! -d "$SRC/i486-linux-musl-cross" ]
then
wget https://musl.cc/i486-linux-musl-cross.tgz
tar xvf i486-linux-musl-cross.tgz
rm i486-linux-musl-cross.tgz
fi
cd $WORK