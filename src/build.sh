#!/bin/bash

source src/env.sh

cd $SRC/linux
make ARCH=x86 bzImage -j $CORES
mv arch/x86/boot/bzImage $OUT
cd $WORK

cd $SRC/busybox/
sed -i "s|.*CONFIG_CROSS_COMPILER_PREFIX.*|CONFIG_CROSS_COMPILER_PREFIX="\"${BASE}"i486-linux-musl-cross/bin/i486-linux-musl-\"|" .config
sed -i "s|.*CONFIG_SYSROOT.*|CONFIG_SYSROOT=\""${BASE}"i486-linux-musl-cross\"|" .config
sed -i "s|.*CONFIG_EXTRA_CFLAGS.*|CONFIG_EXTRA_CFLAGS=-I$BASE/i486-linux-musl-cross/include|" .config
sed -i "s|.*CONFIG_EXTRA_LDFLAGS.*|CONFIG_EXTRA_LDFLAGS=-L$BASE/i486-linux-musl-cross/lib|" .config
make ARCH=x86 -j $CORES
make install

mv $SRC/busybox/_install $OUT/filesystem
cd $OUT/filesystem
mkdir -pv $OUT/filesystem/{dev,proc,etc/init.d,sys,tmp}
sudo mknod $OUT/filesystem/dev/console c 5 1
sudo mknod $OUT/filesystem/dev/null c 1 3
cp $SRC/config/welcome $OUT/filesystem/etc/welcome
cp $SRC/config/profile $OUT/filesystem/etc/profile
cp $SRC/config/hostname $OUT/filesystem/etc/hostname
cp $SRC/config/inittab $OUT/filesystem/etc/inittab
cp $SRC/config/rc $OUT/filesystem/etc/init.d/rc
chmod +x $OUT/filesystem/etc/init.d/rc
sudo chown -R root:root $OUT/filesystem/

find . | cpio -H newc -o | xz --check=crc32 >  $OUT/rootfs.cpio.xz
cd $WORK

if [ ! -f $OUT/syslinux.cfg ]
then
cp $SRC/config/syslinux.cfg  $OUT/syslinux.cfg
fi

if [ ! -f $OUT/$IMG ]
then
dd if=/dev/zero of=$OUT/$IMG bs=1k count=1440
mkdosfs $OUT/$IMG
syslinux --install $OUT/$IMG
fi

if [ ! -d $OUT/mnt ]
then
mkdir $OUT/mnt
fi


sudo mount -o loop $OUT/$IMG $OUT/mnt
sudo cp $OUT/bzImage $OUT/mnt
sudo cp $OUT/rootfs.cpio.xz $OUT/mnt
sudo cp $OUT/syslinux.cfg $OUT/mnt
sudo umount $OUT/mnt
