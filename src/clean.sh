#!/bin/bash

source src/env.sh

rm -rf $OUT/bzImage
rm -rf $OUT/rootfs.cpio.xz
rm -rf $OUT/syslinux.cfg
sudo rm -rf $OUT/filesystem
sudo rm -rf $OUT/mnt