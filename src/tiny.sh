#!/bin/bash

source src/env.sh

cd $SRC/linux
make ARCH=x86 tinyconfig
cd $WORK

cd $SRC/busybox
make ARCH=x86 allnoconfig
cd $WORK